#ifndef _RADIOX10_H
#define _RADIOX10_H
#include "cmsis_os.h"
typedef enum E_SEND_STATES_DURATIONS
{
	SSD_BURST_START   = 16,
	SSD_SILENCE_START = 7,
	SSD_SMALL         = 1,
	SSD_LONG          = 3,
	SSD_END			  = 71
}E_SEND_STATES_DURATIONS;

typedef enum E_SEND_STATES
{
	SSD_NONE,
	SSD_BURST
}E_SEND_STATES;


typedef struct SendState
{
	E_SEND_STATES_DURATIONS burstDuration;
	E_SEND_STATES_DURATIONS silenceDuration;
}SendState;


void on_tim3_interrupt();

void radioX10_App(osMessageQId queueId);

#endif