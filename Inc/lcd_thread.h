#ifndef LCD_THREAD_H_
#define  LCD_THREAD_H_
#include <cmsis_os.h>
#include "temp_datadef.h"
#include "lcd_log.h"
#include "stm32f7xx_hal.h"
#include "stm32746g_discovery.h"
#include "stm32746g_discovery_lcd.h"
#include "stm32746g_discovery_ts.h"
#include <stdbool.h>

void LCDThread(void const* argument);
#endif // !LCD_THREAD_H_

