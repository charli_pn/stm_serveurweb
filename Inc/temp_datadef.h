#ifndef TEMP_DATADEF_
#define TEMP_DATADEF_

#include "cmsis_os.h"
typedef struct
{
	float temp;
	float moist;
}temp_data_t;

typedef struct
{
	temp_data_t* shared_temp_data;
	osMutexId temp_data_mutex_id;
	osMessageQId* lamp_state_req_queue;
}thread_data;
#endif // !TEMP_DATADEF_
