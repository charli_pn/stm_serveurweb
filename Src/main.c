/**
  ******************************************************************************
  * @file    LwIP/LwIP_HTTP_Server_Netconn_RTOS/Src/main.c 
  * @author  MCD Application Team
  * @brief   This sample code implements a http server application based on 
  *          Netconn API of LwIP stack and FreeRTOS. This application uses 
  *          STM32F7xx the ETH HAL API to transmit and receive data. 
  *          The communication is done with a web browser of a remote PC.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2016 STMicroelectronics International N.V. 
  * All rights reserved.</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "ethernetif.h"
#include "lwip/netif.h"
#include "lwip/tcpip.h"
#include "app_ethernet.h"
#include "httpserver-netconn.h"
#include "lcd_log.h"
#include "radioX10_App.h"
#include "stm32f7xx_hal.h"
#include <stdbool.h>
#include "exchange_data_types.h"
#include "temp_datadef.h"
#include "lcd_thread.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
struct netif gnetif; /* network interface structure */

TIM_HandleTypeDef TimHandle3;
uint32_t uwPrescalerValue = 0;
uint8_t phase = 0;

osMessageQDef(lamp_state_req_queue, 50, bool);           // Declare a message queue
osMessageQId lamp_state_req_queue;                   // Declare an ID for the message queue

osPoolDef(temp_data_pool, 1, temp_data_t);   // Declare memory pool
osPoolId(temp_data_pool);                   // Memory pool ID

osMutexDef(temp_data_mutex);     // Declare mutex
osMutexId(temp_data_mutex);   // Mutex ID



/* Private function prototypes -----------------------------------------------*/
static void SystemClock_Config(void);
static void WebThread(void const * argument);
static void LampControlThread(void const * argument);
static void TestThread(void const * argument);
static void Netif_Config(void);
static void MPU_Config(void);
static void Error_Handler(void);
static void CPU_CACHE_Enable(void);


void GpioInit();

void Timer3Init();
void SetupMailQueue();

/* Private functions ---------------------------------------------------------*/

temp_data_t shared_temp_data;
thread_data thread_data_lcd, thread_data_web, thread_data_lamp;

osMutexId temp_data_mutex_id;

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{
	
	
	
	/* Configure the MPU attributes as Device memory for ETH DMA descriptors */
	MPU_Config();

	/* Enable the CPU Cache */
	CPU_CACHE_Enable();

	/* STM32F7xx HAL library initialization:
	     - Configure the Flash ART accelerator on ITCM interface
	     - Configure the Systick to generate an interrupt each 1 msec
	     - Set NVIC Group Priority to 4
	     - Global MSP (MCU Support Package) initialization
	*/
	HAL_Init();  
  
	/* Configure the system clock to 200 MHz */
	SystemClock_Config(); 
  
	/* Init thread */
#if defined(__GNUC__)
	osThreadDef(WEB, WebThread, osPriorityNormal, 0, configMINIMAL_STACK_SIZE * 5);
#else
	osThreadDef(WEB, WebThread, osPriorityNormal, 0, configMINIMAL_STACK_SIZE * 2);
#endif

#if defined(__GNUC__)
	osThreadDef(LAMP, LampControlThread, osPriorityNormal, 0, configMINIMAL_STACK_SIZE * 5);
#else
	osThreadDef(LAMP, LampControlThread, osPriorityNormal, 0, configMINIMAL_STACK_SIZE * 2);
#endif

#if defined(__GNUC__)
	osThreadDef(TEST, TestThread, osPriorityNormal, 0, configMINIMAL_STACK_SIZE * 5);
#else
	osThreadDef(TEST, TestThread, osPriorityNormal, 0, configMINIMAL_STACK_SIZE * 2);
#endif

#if defined(__GNUC__)
	osThreadDef(LCD, LCDThread, osPriorityNormal, 0, configMINIMAL_STACK_SIZE * 5);
#else
	osThreadDef(LCD, LCDThread, osPriorityNormal, 0, configMINIMAL_STACK_SIZE * 2);
#endif



	lamp_state_req_queue = osMessageCreate(osMessageQ(lamp_state_req_queue), NULL);
	temp_data_mutex_id = osMutexCreate(osMutex(temp_data_mutex));
//	shared_temp_data = (temp_data_t *) osPoolAlloc(temp_data_pool);

	shared_temp_data.temp = 5;

	
	thread_data_lcd.lamp_state_req_queue = &lamp_state_req_queue;
	
	thread_data_web.lamp_state_req_queue = &lamp_state_req_queue;
	thread_data_web.shared_temp_data = &shared_temp_data;
	thread_data_web.temp_data_mutex_id = temp_data_mutex_id;
	
	thread_data_lamp.lamp_state_req_queue = &lamp_state_req_queue;
	thread_data_lamp.shared_temp_data = &shared_temp_data;
	thread_data_lamp.temp_data_mutex_id = temp_data_mutex_id;
	
	//osThreadCreate(osThread(TEST), NULL);
	osThreadCreate(osThread(LCD), (void*)&thread_data_lcd);
	osThreadCreate(osThread(WEB), (void*)&thread_data_web);
	osThreadCreate(osThread(LAMP), (void*)&thread_data_lamp);
	
	
	/* Start scheduler */
	osKernelStart();
  
	/* We should never get here as control is now taken by the scheduler */
	for (;;)
	{
	}
}


void SetupMailQueue()
{
	

}

/**
  * @brief  Start Thread 
  * @param  argument not used
  * @retval None
  */
static void WebThread(void const * argument)
{ 
	/* Create tcp_ip stack thread */
	tcpip_init(NULL, NULL);
  
	/* Initialize the LwIP stack */
	Netif_Config();
  
	/* Initialize webserver demo */
	http_server_netconn_init((thread_data*)argument);
  
	/* Notify user about the network interface config */
//	User_notification(&gnetif);
  
#ifdef USE_DHCP
	/* Start DHCPClient */
	osThreadDef(DHCP, DHCP_thread, osPriorityBelowNormal, 0, configMINIMAL_STACK_SIZE * 2);
	osThreadCreate(osThread(DHCP), &gnetif);
#endif

	for (;;)
	{
		osDelay(10);
		/* Delete the Init Thread */ 
		osThreadTerminate(NULL);
	}
}


void LampControlThread(void const* argument)
{
	GpioInit();
	Timer3Init();

	thread_data* data = (thread_data*)argument;
	radioX10_App(*data->lamp_state_req_queue);

	for (;;)
	{
		osDelay(10);
		/* Delete the Init Thread */ 
		//osThreadTerminate(NULL);
	}
}


void TestThread(void const* argument)
{
	osDelay(1000);
	osMessagePut(lamp_state_req_queue, true, 100);
	osDelay(1000);
	osMessagePut(lamp_state_req_queue, false, 100);
	osDelay(1000);
	osMessagePut(lamp_state_req_queue, true, 100);
	osDelay(1000);
	osMessagePut(lamp_state_req_queue, false, 100);

	for (;;)
	{
		/* Delete the Init Thread */ 
		osThreadTerminate(NULL);
	}
}


/**
  * @brief  Initializes the lwIP stack
  * @param  None
  * @retval None
  */



static void Netif_Config(void)
{ 
	ip_addr_t ipaddr;
	ip_addr_t netmask;
	ip_addr_t gw;
 
#ifdef USE_DHCP
	ip_addr_set_zero_ip4(&ipaddr);
	ip_addr_set_zero_ip4(&netmask);
	ip_addr_set_zero_ip4(&gw);
#else
	IP_ADDR4(&ipaddr, IP_ADDR0, IP_ADDR1, IP_ADDR2, IP_ADDR3);
	IP_ADDR4(&netmask, NETMASK_ADDR0, NETMASK_ADDR1, NETMASK_ADDR2, NETMASK_ADDR3);
	IP_ADDR4(&gw, GW_ADDR0, GW_ADDR1, GW_ADDR2, GW_ADDR3);
#endif /* USE_DHCP */
  
	netif_add(&gnetif, &ipaddr, &netmask, &gw, NULL, &ethernetif_init, &tcpip_input);
  
	/*  Registers the default network interface. */
	netif_set_default(&gnetif);
  
	if (netif_is_link_up(&gnetif))
	{
		/* When the netif is fully configured this function must be called.*/
		netif_set_up(&gnetif);
	}
	else
	{
		/* When the netif link is down this function must be called */
		netif_set_down(&gnetif);
	}
}

/**
  * @brief  Initializes the STM327546G-Discovery's LCD  resources.
  * @param  None
  * @retval None
  */
/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow : 
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 200000000
  *            HCLK(Hz)                       = 200000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 4
  *            APB2 Prescaler                 = 2
  *            HSE Frequency(Hz)              = 25000000
  *            PLL_M                          = 25
  *            PLL_N                          = 432
  *            PLL_P                          = 2
  *            PLL_Q                          = 9
  *            VDD(V)                         = 3.3
  *            Main regulator output voltage  = Scale1 mode
  *            Flash Latency(WS)              = 7
  * @param  None
  * @retval None
  */
static void SystemClock_Config(void)
{
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_OscInitTypeDef RCC_OscInitStruct;

	/* Enable HSE Oscillator and activate PLL with HSE as source */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.HSIState = RCC_HSI_OFF;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 25;
	RCC_OscInitStruct.PLL.PLLN = 400;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 9;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		Error_Handler();
	}

	/* activate the OverDrive */
	if (HAL_PWREx_EnableOverDrive() != HAL_OK)
	{
		Error_Handler();
	}
  
	/* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 
	   clocks dividers */
	RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;  
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;  
	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7) != HAL_OK)
	{
		Error_Handler();
	}
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
static void Error_Handler(void)
{
	/* User may add here some code to deal with this error */
	while (1)
	{
	}
}

/**
  * @brief  Configure the MPU attributes .
  * @param  None
  * @retval None
  */
static void MPU_Config(void)
{
	MPU_Region_InitTypeDef MPU_InitStruct;
  
	/* Disable the MPU */
	HAL_MPU_Disable();
  
	/* Configure the MPU as Normal Non Cacheable for Ethernet Buffers in the SRAM2 */
	MPU_InitStruct.Enable = MPU_REGION_ENABLE;
	MPU_InitStruct.BaseAddress = 0x2004C000;
	MPU_InitStruct.Size = MPU_REGION_SIZE_16KB;
	MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
	MPU_InitStruct.IsBufferable = MPU_ACCESS_NOT_BUFFERABLE;
	MPU_InitStruct.IsCacheable = MPU_ACCESS_NOT_CACHEABLE;
	MPU_InitStruct.IsShareable = MPU_ACCESS_SHAREABLE;
	MPU_InitStruct.Number = MPU_REGION_NUMBER0;
	MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL1;
	MPU_InitStruct.SubRegionDisable = 0x00;
	MPU_InitStruct.DisableExec = MPU_INSTRUCTION_ACCESS_ENABLE;

	HAL_MPU_ConfigRegion(&MPU_InitStruct);
  
	/* Configure the MPU as Device for Ethernet Descriptors in the SRAM2 */
	MPU_InitStruct.Enable = MPU_REGION_ENABLE;
	MPU_InitStruct.BaseAddress = 0x2004C000;
	MPU_InitStruct.Size = MPU_REGION_SIZE_256B;
	MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
	MPU_InitStruct.IsBufferable = MPU_ACCESS_BUFFERABLE;
	MPU_InitStruct.IsCacheable = MPU_ACCESS_NOT_CACHEABLE;
	MPU_InitStruct.IsShareable = MPU_ACCESS_SHAREABLE;
	MPU_InitStruct.Number = MPU_REGION_NUMBER1;
	MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL0;
	MPU_InitStruct.SubRegionDisable = 0x00;
	MPU_InitStruct.DisableExec = MPU_INSTRUCTION_ACCESS_ENABLE;

	HAL_MPU_ConfigRegion(&MPU_InitStruct);
  
	/* Enable the MPU */
	HAL_MPU_Enable(MPU_PRIVILEGED_DEFAULT);
}

/**
  * @brief  CPU L1-Cache enable.
  * @param  None
  * @retval None
  */
static void CPU_CACHE_Enable(void)
{
	/* Enable I-Cache */
	SCB_EnableICache();

	/* Enable D-Cache */
	SCB_EnableDCache();
}

void GpioInit()
{
	GPIO_InitTypeDef GPIO_InitStruct;
	
	__GPIOB_CLK_ENABLE();
	
	//Configure GPIO pin : P0 *
	GPIO_InitStruct.Pin = GPIO_PIN_14;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	// digital Output
	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
}

void Timer3Init()
{
	/* Set TIMx instance */
	TimHandle3.Instance = TIM3;
	TimHandle3.Init.Period = 563;
	TimHandle3.Init.Prescaler = 99;
	TimHandle3.Init.ClockDivision = 0;
	TimHandle3.Init.CounterMode = TIM_COUNTERMODE_UP;

	//configure timer
	if(HAL_TIM_Base_Init(&TimHandle3) != HAL_OK)
	{
		while (1) 
		{

		}
	}
  
	//##-2- Start the TIM Base generation in interrupt mode ####################*/
	/* Start Channel1 */
	if(HAL_TIM_Base_Start_IT(&TimHandle3) != HAL_OK)
	{
		while (1) 
		{

		}
	}
}



#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
	/* User can add his own implementation to report the file name and line number,
	   ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	 /* Infinite loop */
	while (1)
	{
	}
}
#endif

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
