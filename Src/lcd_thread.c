#include "lcd_thread.h"
#include <math.h> 
#include <stdlib.h>
#include <stdio.h> 

static TS_StateTypeDef  TS_State;

osMessageQId lamp_state_req_queue;

extern temp_data_t shared_temp_data;

void drawMoistAndTemp();

void LCDThread(void const* argument)
{

	thread_data* data = (thread_data*)argument;
	osMutexId mutexData = data->temp_data_mutex_id;
	
	lamp_state_req_queue = *data->lamp_state_req_queue;
//	shared_temp_data = data->shared_temp_data;
	
	uint16_t x, y;
	/* Initialize the LCD */

	BSP_LCD_Init();
	BSP_LCD_LayerDefaultInit(1, LCD_FB_START_ADDRESS);
	BSP_TS_Init(BSP_LCD_GetXSize(), BSP_LCD_GetYSize());
  
	/* Set LCD Foreground Layer  */
	BSP_LCD_SelectLayer(1);
  
	BSP_LCD_SetFont(&LCD_DEFAULT_FONT);
	
	/* Clear the LCD */
	BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
	BSP_LCD_Clear(LCD_COLOR_WHITE);
	/* Set Header */
	BSP_LCD_SetTextColor(LCD_COLOR_LIGHTMAGENTA);
	BSP_LCD_FillRect(0, 0, BSP_LCD_GetXSize(), 80);
	BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
	BSP_LCD_SetBackColor(LCD_COLOR_LIGHTMAGENTA);
	BSP_LCD_SetFont(&Font24);
	BSP_LCD_DisplayStringAt(0, 30, (uint8_t *)"Que la lumiere soit !", CENTER_MODE);

	
	BSP_LCD_SetBackColor(LCD_COLOR_LIGHTGRAY);
	BSP_LCD_SetTextColor(LCD_COLOR_LIGHTGRAY);
	BSP_LCD_FillCircle(100, 150, 50);
	BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
	BSP_LCD_DisplayStringAt(65, 140, "JOUR", LEFT_MODE);

	BSP_LCD_SetTextColor(LCD_COLOR_LIGHTGRAY);
	BSP_LCD_FillCircle(220, 150, 50);
	BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
	BSP_LCD_DisplayStringAt(185, 140, "NUIT", LEFT_MODE);
	                   
	while (1)
	{
		osDelay(50);   //  ~20fps
//		osMutexWait(mutexData, osWaitForever);
		drawMoistAndTemp();
//		osMutexRelease(mutexData);
		BSP_TS_GetState(&TS_State);
		if (TS_State.touchDetected)
		{
			/* Get X and Y position of the touch post calibrated */
			x = TS_State.touchX[0];
			y = TS_State.touchY[0];
				
			if (y<200 && y> 100)
			{
				if (x > 50 && x < 150)
				{
					// TODO : Allumer la lumière
					osMessagePut(lamp_state_req_queue, true, 100);
					osDelay(500);  //0.5s cooldown
				}
				if (x > 170 && x < 270)
				{
					// TODO : Eteindre la lumière
					osMessagePut(lamp_state_req_queue, false, 100);
					osDelay(500);   //0.5s cooldown
				}
			}
		}
	}
	
  
	/* Initialize LCD Log module */
	//LCD_LOG_Init();
  
  /* Show Header and Footer texts */
  //LCD_LOG_SetHeader((uint8_t *)"Webserver Application Netconn API");
  //LCD_LOG_SetFooter((uint8_t *)"STM32746G-DISCO board");
  
  //LCD_UsrLog ((char *)"  State: Ethernet Initialization ...\n");
}

void drawMoistAndTemp()
{
	char *temp, *moist;
	sprintf(temp, "%.1f", shared_temp_data.temp);
	sprintf(moist, "%.1f", shared_temp_data.moist);
	BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
	BSP_LCD_SetTextColor(LCD_COLOR_MAGENTA);
	BSP_LCD_DisplayStringAt(310, 110, (uint8_t *)"Temp :", LEFT_MODE);
	BSP_LCD_DisplayStringAt(310, 140, temp, LEFT_MODE);
	BSP_LCD_DisplayStringAt(310, 170, (uint8_t *)"Moist :", LEFT_MODE);
	BSP_LCD_DisplayStringAt(310, 200, moist, LEFT_MODE);
	
}