#include <radioX10_App.h>
#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include "stm32f7xx_hal.h"

#define BUFFER_SIZE 2048
#define REPEAT_MSG 5

osMessageQId lamp_state_req_queue;

SendState send_buffer[BUFFER_SIZE];
uint16_t curr_buffer_id = 0;
uint16_t curr_buffer_size = 0;
uint16_t overflowCounter = 0;

uint8_t MSG_ON = 0x00;
uint8_t MSG_OFF = 0x20;


//osMessageQId lampReqQueue;


bool isInit = false;
E_SEND_STATES_DURATIONS curr_wait_time;
E_SEND_STATES curr_state;

SendState createSendState(E_SEND_STATES_DURATIONS burstDuration, E_SEND_STATES_DURATIONS silenceDuration);
void set_rf_out_value(uint8_t value);

void send_radio_message(const uint8_t addr, const uint8_t msg);
void append_data_to_buffer(uint16_t* id, uint8_t value);
void setLamp1State(bool active);




void radioX10_App(osMessageQId queueId) {

	isInit  = true;

	curr_state = SSD_NONE;
	curr_wait_time = 0;

//	setLamp1State(true);
	while (1)
	{
		const osEvent evt = osMessageGet(lamp_state_req_queue, osWaitForever);      // wait for message
		if(evt.status == osEventMessage) {
			const bool val = evt.value.v;
			setLamp1State(val);
		}
	}
}

bool value = true;

void setLamp1State(bool active)
{
	uint8_t msg = active ? MSG_ON : MSG_OFF;
	for (size_t i = 0; i < REPEAT_MSG; i++)
	{
		send_radio_message(0x60, msg);
	}
}


void on_tim3_interrupt()
{
	if (!isInit) return;
	overflowCounter++;
	if (overflowCounter >= curr_wait_time)
	{
		overflowCounter = 0;
	}
	else return;
	
	if (curr_buffer_size > 0)
	{
		SendState currState = send_buffer[curr_buffer_id];
		switch (curr_state) {
		case SSD_NONE:
			curr_state = SSD_BURST;
			set_rf_out_value(1);
			curr_wait_time = currState.burstDuration;
			break;
		case SSD_BURST:
			curr_state = SSD_NONE;
			set_rf_out_value(0);
			curr_wait_time = currState.silenceDuration;

			curr_buffer_id++;
			curr_buffer_id %= BUFFER_SIZE;
			curr_buffer_size--;
			break;
		default:
			break;
		}
	}
	else
	{
		curr_wait_time = 0;
	}
	
	
}

void set_rf_out_value(uint8_t value)
{
	//TODO out GPIO
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, value);
}

void send_radio_message(const uint8_t addr, const uint8_t msg)
{
	int msgSize = 34;


	if (curr_buffer_size + msgSize > BUFFER_SIZE)
		return;

	uint16_t id = curr_buffer_id + curr_buffer_size;
	

	//start
	send_buffer[id] = createSendState(SSD_BURST_START, SSD_SILENCE_START);
	id++;
	//set addr
	append_data_to_buffer(&id, addr);
	
	//inverse addr
	const uint8_t invAddr = ~addr;
	append_data_to_buffer(&id, invAddr);

	//set data
	append_data_to_buffer(&id, msg);
	
	//inverse data
	const uint8_t invMsg = ~msg;
	append_data_to_buffer(&id, invMsg);

	//end bit with 40ms pause
	send_buffer[id] = createSendState(SSD_SMALL, SSD_END);
	
	curr_buffer_size += msgSize;
}

void append_data_to_buffer(uint16_t* id, uint8_t value)
{
	for (size_t i = 0; i < 8; i++)
	{
		bool isOne = (value & (1 << (7 - i))) != 0;

		send_buffer[*id] = createSendState(SSD_SMALL, isOne ? SSD_LONG : SSD_SMALL);
		
		
		(*id)++;
		(*id) %= BUFFER_SIZE;
	}
}


SendState createSendState(E_SEND_STATES_DURATIONS burstDuration, E_SEND_STATES_DURATIONS silenceDuration)
{
	SendState ret;
	ret.burstDuration = burstDuration;
	ret.silenceDuration = silenceDuration;
	return ret;
}

/*************************** End of file ****************************/
